# React Native maps && Directions

Simple react native project that uses maps && directions API with multipe directions.

## Installation

```bash
npm install
```
### or

```bash
yarn install (recommended)
```

## Run it on your devices

[https://reactnative.dev/docs/running-on-device](https://reactnative.dev/docs/running-on-device)
