import React from "react";
import { FlatList, SafeAreaView, StatusBar, StyleSheet, Text, TouchableOpacity } from "react-native";
import ROUTES from './data/routes.json'


const Item = ({ item, onPress }) => (

    <TouchableOpacity onPress={onPress} style={styles.item}>
        <Text style={styles.title}>{item.description}</Text>
    </TouchableOpacity>
);

const PlacesComponent = ({ navigation }) => {

    const renderItem = ({ item }) => {

        return (
            <Item
                item={item}
                // SEND DATA TO MapComponent
                onPress={() => navigation.navigate('Map', {
                    routesToGo: item,
                })}
            />
        );
    };

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={ROUTES}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}

            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        backgroundColor: 'cyan',
    },
    title: {
        fontSize: 32,
    },
});

export default PlacesComponent;