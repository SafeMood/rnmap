import React, { useState } from "react";
import { Dimensions, StyleSheet, View } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 30.050555088287382;
const LONGITUDE = 31.234428925107082;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

// PUT IT IN A CONFIG OR  ENV FILE  
// it require billing activation
const GOOGLE_MAPS_APIKEY = 'YOUR_GOOGLE_MAPS_APIKEY';



function MapComponent({ route }) {
    const [mapView, setMapView] = useState(null);

    // RETRIEVE DATA FROM NAVIFATION ROUTE
    const { routesToGo } = route.params;


    // YOU CAN IMPROVE THIS TO GIVE UNIQUE COLORS EVERYTIME
    const _getRandomColor = () => {
        const COLORS = [
            'red',
            'blue',
            'yellow',
            'black',
            'chartreuse',
            'cyan',
        ]
        return COLORS[Math.floor(Math.random() * COLORS.length)];
    }

    const renderMapDirections = (routes) => {
        // this will create a new array of our elements 
        return routes.result.map((item, index) => {

            const origin = { latitude: item.pickupStation.latitude, longitude: item.pickupStation.longitude };
            const originDescription = item.pickupStation.description
            const destination = { latitude: item.dropoffStation.latitude, longitude: item.dropoffStation.longitude };
            const destinationDescription = item.dropoffStation.description;
            const vehicles = item.vehicle;
            const walkingTimeToDestination = item.walkingTimeToDestination;
            const travelTimeFromPickupToDropoff = item.travelTimeFromPickupToDropoff;

            return (
                <React.Fragment key={index.toString()} >
                    <Marker
                        coordinate={origin}
                        description={originDescription}
                    />
                    <Marker
                        coordinate={destination}
                        description={destinationDescription}
                    />
                    <MapViewDirections

                        origin={origin}
                        destination={destination}
                        apikey={GOOGLE_MAPS_APIKEY}
                        strokeWidth={3}
                        strokeColor={_getRandomColor()}
                    />
                </React.Fragment>
            )
        })
    }

    onReady = (result) => {
        mapView.fitToCoordinates(result.coordinates, {
            edgePadding: {
                right: (width / 10),
                bottom: (height / 10),
                left: (width / 10),
                top: (height / 10),
            },
        });
    }

    onError = (errorMessage) => {
        console.log(errorMessage); // eslint-disable-line no-console
    }



    return (
        <View style={StyleSheet.absoluteFill}>
            <MapView
                initialRegion={{
                    latitude: LATITUDE,
                    longitude: LONGITUDE,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }}
                style={StyleSheet.absoluteFill}
                ref={c => setMapView(c)} // eslint-disable-line react/jsx-no-bind
            >
                {renderMapDirections(routesToGo)}
            </MapView>

        </View>
    );
}

export default MapComponent;

