import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PlacesComponent from './src/PlacesComponent';
import MapComponent from './src/MapComponent';


export default function App() {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Places" component={PlacesComponent} />
        <Stack.Screen name="Map" component={MapComponent} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

